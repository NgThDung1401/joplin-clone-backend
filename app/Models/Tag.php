<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Tag extends Model
{
    use HasFactory;

    protected $fillable = ['label'];

    public function notes(): BelongsToMany {
        return $this->belongsToMany(Note::class, 'tags_notes', 'tag_id', 'note_id');
    }
}
